<?php

/**
 * @file
 * Configuration forms and helper functions for Node points module.
 */

/**
 * Administrative settings for Node points.
 */
function node_points_settings_form($form_state) {

  $ff = array(
    array('body','Body'),
    array('comments','Comments'),
    array('taxonomy','Taxonomy'),
    array('statistics','Statistics'),
  );

 foreach($ff as $f) {
    $k = $f[0]; $i = $f[1];

    $form[$k] = array(
    '#type' => 'fieldset', 
    //  '#tree' => TRUE,
      '#title' => t($i), 
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,  
    );
  
    $form[$k]['node_points_'.$k] = array(
      '#type' => 'textarea',
      '#title' => t($i),
      '#tree' => false,
      '#description' => t('The points based on the '.$k.'. For example: <300:45 or >120:12'),
      '#default_value' => variable_get('node_points_'.$k),
      '#cols' => 3, 
      '#rows' => 2,
    );
  
    $form[$k]['node_points_'.$k.'_multiplier'] = array(
      '#type' => 'textfield',
      '#tree' => false,
      '#title' => t($i.' multiplier'),
      '#description' => t('For example: 1.3'),
      '#default_value' => variable_get('node_points_'.$k.'_multiplier'),
      '#size' => 2,
      '#maxlength' => 5,
    );
  }



  return system_settings_form($form);
}

